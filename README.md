[TOC]

**解决方案介绍**
===============
该解决方案基于华为云函数工作流FunctionGraph的APIG触发器，来帮助用户实现一个无服务器的GraphQL API，用户通过这个API查询或更新数据。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/serverless-graphql-implementation.html

**架构图**
---------------
![方案架构](./document/serverless-graphql-implementation.PNG)

**架构描述**
---------------
该解决方案会部署如下资源：
- 在函数工作流 FunctionGraph上创建一个graphql函数和APIG触发器，实现一个无服务器的GraphQL API。
- API网关 APIG，应用将自身的服务能力封装成API，并通过API网关开放给用户调用。

**组织结构**
---------------
``` lua
huaweicloud-solution-serverless-graphql-implementation
├── serverless-graphql-implementation.tf.json -- 资源编排模板
├── functiongraph
    ├── bootstrap     -- 函数执行入口
	├── server.js     -- 启动graphql http server
    ├── node_modules  -- 存储项目所依赖的外部模块
    ├── package.json  -- 项目信息文件


```
**开始使用**
---------------

1、登录[华为云控制台](https://console.huaweicloud.com/console/?locale=zh-cn&region=cn-north-4#/home)，区域选择“华北-北京四”

图1 华为云控制台

![华为云控制台](./document/readme-image-001.PNG)

2、进入[函数工作流 FunctionGraph](https://console.huaweicloud.com/functiongraph/?region=cn-north-4#/serverless/dashboard)控制台，在函数列表中查看该方案创建的函数

图2 创建的函数

![创建的函数](./document/readme-image-002.PNG)

3、进入创建的函数中，查看该方案创建的触发器信息。

图3 graphql函数触发器

![视频转码函数触发器](./document/readme-image-003.PNG)

4、点击APIG触发器进入API界面，然后点击调试按钮，进入调试界面。

图4 API界面

![API界面](./document/readme-image-004.PNG)

5、在该API的调试界面调试graphql变更操作createMessage创建一条消息，调试结果如下：

图5 调试结果

![调试结果](./document/readme-image-005.PNG)

请求体：
``` lua
{
	"query": "mutation CreateMessage($input: MessageInput) {createMessage(input: $input) {id}}",
	"variables": {
		"input": {
			"author": "andy",
			"content": "hope is a good thing"
		}
	}
}
```

6、在该API的调试界面调试graphql查询操作getMessage查询上一步创建的消息，调试结果如下：

图6 调试结果

![调试结果](./document/readme-image-006.PNG)

请求体：
``` lua
{
	"query": "query GetMessage($id: ID!) {getMessage(id: $id) {id,content,author}}",
	"variables": {
		"id": "d4b896b0be870f2845d4"
	}
}
```